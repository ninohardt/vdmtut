<style>
.midcenter {
    position: fixed;
    top: 50%;
    left: 50%;
}
</style>

Volumetric Conjoint
========================================================
author: Nino Hardt
date: 2018
width: 1450
height: 900



This tutorial
========================================================

<div class="midcenter" style="margin-left:-300px; margin-top:-300px;">
<img src="tutscr.png"></img>
</div>



This tutorial
========================================================

- The presentation is in the .rpres format, and you have access to its source
- As we walk through the slides, you can run individual code snippets, run all code until a specific point, or copy and modify the code if you want
- Everything can be reproduced
- Download files from vdmtut repository link on https://goo.gl/htAADL



Overview
========================================================

- The `echoice` package
- Modeling Volumetric Demand
- Model Estimation
- Example 1: Frozen Pizza
  + Estimation, Fit, Market simulation
- Example 2: Ice cream
  + Estimation, Fit, Elasticities, demand curves
- Exploded Logit



echoice package
====================================
type: section



echoice
========================================================
incremental: true
- The package is called `echoice` - econ-based choice models
- It implements the popular volumetric demand model, including the MCMC sampler for estimation, demand simulators, and other tools
- Under active development, available from public repository
- Installing from the bitbucket repository ensures you have the most recent version
- I am open to feature requests, feedback ...



Package installation
========================================================

- Most up-to-date development version available on bitbucket <https://bitbucket.org/ninohardt/echoice/>.
- Installation via devtools package


```r
library(devtools)
install_bitbucket("ninohardt/echoice", ref = "master")
```

- Alternatively, download and install binary version

All files here:
https://goo.gl/htAADL



Package installation on OSX
========================================================
incremental: true

- You may need to install 'xcode command line tools' (CLI) -- usually Rstudio will remind you if necessary
- If xcode CLI needs to be installed, open the OSX Terminal, enter `xcode-select --install`
- If you see an error message mentioning `gfortran` upon installing a source packag (or compiling other Rcpp code), you may need to install gfortran 6.3 <http://gcc.gnu.org/wiki/GFortranBinaries#MacOS>
- For more information on OSX-related issues with compilation, see <https://thecoatlessprofessor.com/programming/rcpp-rcpparmadillo-and-os-x-mavericks--lgfortran-and--lquadmath-error/>
- Alternatively, use a pre-compiled binary version of the package



Load packages used in this presentation
========================================================


```r
library(echoice)
library(knitr)
library(reshape2)
```




Volumetric Demand
====================================
type: section


Conjoint and transaction data
========================================================

- Volumetric Conjoint allows purchase of any quantity of any alternative/good (just like actual purchases)
- Choice tasks usually involve showing less alternatives than are available on the market
- Part-worth estimates from conjoint should be similar to those obtained from transaction data (shown last year at ART)




Conjoint and transaction data (2)
========================================================
incremental: true

![SP-RP consistency](sprp.png)

***

- We estimated the same model specification with the same set of attributes on Volumetric Conjoint and Store Transaction Data from the same individuals
- While baseline marginal utility of inside goods, satiation and budget differed, marginal utilities from attribute (part-worths) turned out consistent




Volumetric Conjoint
========================================================

<div class="midcenter" style="margin-left:-300px; margin-top:-300px;">
<img src="choice_screen.png"></img>
</div>




Challenges of modeling volumetric demand
========================================================
incremental: true

- Budget constraints --> Primary demand (how much to consume) 
- Satiation --> Demand for variety 
- Trading quality and quantity -- does it make sense to introduce 'premium' products, smaller packages with more varieties?
- Should flavor varieties be line-priced?



Do I need volumetric conjoint?
========================================================
incremental: true

- Goal?
- Volume prediction vs share?
- General prioritization of attributes (part-worth) sufficient or not?
- Role of satiation?
- Substitution between quantity and quality?



Modeling volumetric demand
========================================================

- The approach presented here is based on using economic models for analyzing this data
- This matters because we want to predict various counterfactual scenarios
- This may include changes in features, prices and adding or removing options




Volumetric Demand Model
====================================
type: section




The Model: Utility specification
========================================================

We assume that subjects maximize their utility from consuming inside goods $x$ and the outside good $z$:

\begin{equation}
u\left( {{\bf{x}},z} \right) = \sum\limits_k {\frac{{{\psi _k}}}{\gamma }} \ln \left( {\gamma {x_k} + 1} \right) + \ln (z)
\end{equation}

subject to a budget constraint
\[\sum\limits_k^{} {{p_k}} {x_k} + z = E\]

where $\psi_k$ is the baseline utlity of the $k$-th good, and$\gamma$ is a parameter that governs the rate of satiation of the good

We assume that
\[\psi_k = \exp[a_k'\beta + \varepsilon_k]\]




The Model: Marginal Utility
========================================================

The marginal utility for the inside and outside goods is:

\[
\begin{array}{*{20}{l}}
{{u_j}}&{ = \frac{{\partial u\left( {{\bf{x}},z} \right)}}{{\partial {x_j}}} = \frac{{{\psi _j}}}{{\gamma {x_j} + 1}}{\rm{  }}}\\
{{u_z}}&{ = \frac{{\partial u\left( {{\bf{x}},z} \right)}}{{\partial z}} = \frac{1}{z}}
\end{array}
\]



The Model: Kuhn-Tucker
========================================================

Solving for $\varepsilon_j$ leads to the following expression for the KT conditions:

\[\begin{array}{*{20}{l}}
{}&{{\varepsilon _j} = {g_j}\quad {\rm{if}}\quad {x_j} > 0}\\
{}&{{\varepsilon _j} < {g_j}\quad {\rm{if}}\quad {x_j} = 0}
\end{array}\]

where
\[{g_j} =  - {{\bf{a}}_{j'}}\beta  + \ln (\gamma {x_j} + 1) + \ln \left( {\frac{{{p_j}}}{{E - {{\bf{p}}^\prime }{\bf{x}}}}} \right)\]

- Density contributions to the likelihood: ${\varepsilon _j} = {g_j}$
- Mass contributions to the likelihood: ${\varepsilon _j} < {g_j}$





The Model: Towards the Likelihood
========================================================

- We assume extreme-value errors 
- We are able to obtain a closed-form expression for the probability that $R$ of $N$ goods are chosen.  

\[ x_1,x_2,\dots,x_R > 0, \qquad x_{R+1}, x_{R+2}, \dots, x_N = 0. \]

- The error scale $(\sigma$) is identified in this model because price enters the specification without a separate price coefficient
- The likelihood $\ell(\theta)$ of the model parameters is proportional to the probability of observing $n_1$ chosen goods and $n_2$ goods with zero demand 
- The contribution to the likelihood of the chosen goods is in the form of a probability density  while the goods not chosen contribute as a probability mass



The Model: Likelihood
========================================================

- Jacobian from 'change of variables', mass and density contribution

\[l\left( \theta  \right) \propto |{J_R}|\left\{ {\prod\limits_{j = 1}^R {\frac{{\exp ( - {g_j}/\sigma )}}{\sigma }} \exp \left( { - {e^{ - {g_j}/\sigma }}} \right)} \right\}\left\{ {\prod\limits_{i = R + 1}^N {\exp } \left( { - {e^{ - {g_i}/\sigma }}} \right)} \right\}{\rm{ }}\]

- Simplified to:

\[l\left( \theta  \right) \propto {J_R}|\left\{ {\prod\limits_{j = 1}^R {\frac{{\exp ( - {g_j}/\sigma )}}{\sigma }} } \right\}\exp \left\{ { - \sum\limits_{i = 1}^N {\exp } ( - {g_i}/\sigma )} \right\}\]


where $|J_{R}|$ is the Jacobian of the transformation from random-utility error ($\varepsilon$) to the likelihood of the observed data 

In this model, the Jacobian is:
\[\left| {{J_R}} \right| = \prod\limits_{k = 1}^R {\left( {\frac{\gamma }{{\gamma {x_k} + 1}}} \right)} \left\{ {\sum\limits_{k = 1}^R {\frac{{\gamma {x_k} + 1}}{\gamma }} \cdot\frac{{{p_k}}}{{E - {{\bf{p}}^\prime }{\bf{x}}}} + 1} \right\}\]




The Model: Likelihood - Implementation
========================================================

- `vdmevs_ll(voldatai, theta)`: Likelihood for one units/households
  + voldatai: one unit's choices
  + theta: parameter vector
  
- `vdmevs_ll_N(voldata, thetaM)`: Likelihood for several units
  + voldata: list of individual unit's lists containing choice information
  + thetaM: Matrix, one row for each unit


```r
voldatai  = voldata_demo$voldata[[1]]
theta = c(rnorm(18),4,0)
vdmevs_ll(voldatai, theta)
```

```
[1] -4230.674
```



Predicting expected demand
========================================================
incremental: true

- There are no closed form solutions
- Conditional on 1 realization of the error term, we can compute (utility-optimal) demand. This is implemented in `vdm_demand`. It uses an iterative algorithm.
- The error term can only be integrated out numerically
- Additionally, we need to numerically integrate of the posterior distribution of parameters
- `VDMevs_demand` returns draws of the posterior distribution of demand, given draws of the parameter and the offerings as defined by their attributes and prices
- This function will call `vdm_demand` many times, and it may take several minutes to complete (depending on sample size and number of draws used)
- In summary, you need
  + draws of $\theta$
  + a list containing attributes A, prices P




Model estimation
====================================
type: section



Data
========================================================

- two sample datasets
  + simulated frozen pizza dataset (conjoint)
  + real ice cream data set (conjoint)



Data organization
========================================================
incremental: true

- `voldata` format mimics `bayesm`'s `lgtdata` format
- it is a list of lists
- one unit (household) - one list entry
- within the list, the following elements
  * `X` -- quantities, ntask x nalt
  * `P` -- prices, ntask x nalt
  * `A` -- attributes, nalt * ntask x nattr




Data organization: A
========================================================

  * `A` -- attributes, nalt * ntask x nattr - stacked design matrices


```r
data.frame(task=rep(1:4,each=3),alt=rep(1:3,4),bl=1,
           attr1=sample(0:1,12,T),attr2=sample(0:1,12,T))
```

```
   task alt bl attr1 attr2
1     1   1  1     0     0
2     1   2  1     0     1
3     1   3  1     1     0
4     2   1  1     0     0
5     2   2  1     0     0
6     2   3  1     1     1
7     3   1  1     1     1
8     3   2  1     1     1
9     3   3  1     0     0
10    4   1  1     1     0
11    4   2  1     0     0
12    4   3  1     0     0
```




Import helper vd_stacked2list
========================================================

- `vd_stacked2list(sdf)` helps import data from stacked format into voldata format
- data `(sdf)` needs to contain the following columns
  + ID
  + task number
  + product number / choice alternative #
  + quantity
  + price
  + attributes



Import helper vd_stacked2list - example of stacked format
========================================================

- For illustration, consider stacked version of pizza data
- Attributes configurations in columns 6-22


```r
head(vd_list2stacked(voldata_demo$voldata))[,1:8]
```

```
  ID task alternative x     p V1 Fresc RedBa
1  1    1           1 0 3.375  1     0     0
2  1    1           2 0 2.625  1     1     0
3  1    1           3 0 1.500  1     0     0
4  1    1           4 0 4.250  1     0     1
5  1    1           5 0 4.500  1     0     0
6  1    1           6 0 3.500  1     0     0
```



Input checks
========================================================
- The main estimation function `rVDMevs` performs a few input checks
  + `X`,`P`,`A` need to be matrices (not `data.frame`s), convert using `data.matrix()` if necessary
  + quantities `X` need to be non-negative
  + prices `P` need to be positive
  + dimensionalities need to match, i.e. `nrow(X)*ncol(X)==nrow(A)`
- Users need to make sure that design matrices are set up correctly



Dummy coding
========================================================
- Code will work with dummy or effects coding, though we usually use dummy coding
- iota ('1 column') allows estimation of 'baseline' utility of inside vs outside good
- In discrete choice, iota must not be included when transaction data is modeled where demand is non-zero at every occasion
- However, in volumetric choice, we always have some demand for outside good (even though it can be tiny)



Dummy coding: Pizza Example
========================================================

- Synthetic pizza data is using dummy coding
- Let's take a look at `A`


```r
pizzavd=voldata_demo$voldata
str(pizzavd[[1]]$A)
```

```
 num [1:72, 1:17] 1 1 1 1 1 1 1 1 1 1 ...
 - attr(*, "dimnames")=List of 2
  ..$ : NULL
  ..$ : chr [1:17] "" "Fresc" "RedBa" "Priv" ...
```

```r
colnames(pizzavd[[1]]$A)
```

```
 [1] ""           "Fresc"      "RedBa"      "Priv"       "Tomb"      
 [6] "Tony"       "ForTwo"     "TrCr"       "StufCr"     "RisCr"     
[11] "Cheese"     "Veg"        "Surp"       "PepSauHam"  "HI"        
[16] "densetop"   "realcheese"
```




Dummy coding: Pizza Example
========================================================



```r
head(pizzavd[[1]]$A)
```

```
       Fresc RedBa Priv Tomb Tony ForTwo TrCr StufCr RisCr Cheese Veg Surp
[1,] 1     0     0    0    0    1      1    0      0     1      0   0    1
[2,] 1     1     0    0    0    0      1    0      0     0      0   0    0
[3,] 1     0     0    0    0    0      0    1      0     0      0   1    0
[4,] 1     0     1    0    0    0      0    0      1     0      0   0    0
[5,] 1     0     0    0    1    0      1    0      1     0      0   0    0
[6,] 1     0     0    1    0    0      0    0      0     0      1   0    0
     PepSauHam HI densetop realcheese
[1,]         0  0        1          0
[2,]         1  0        0          1
[3,]         0  0        0          1
[4,]         0  0        1          0
[5,]         0  1        1          0
[6,]         0  0        0          1
```




Dummy coding: Pizza Example
========================================================


```r
library(magrittr)
pizzalevels=
list(Brand=c("DiGiorno",
            colnames(pizzavd[[1]]$A)[2:6]),
     Size=c("One",
            colnames(pizzavd[[1]]$A)[7]),
     Crust=c("Thin",
             colnames(pizzavd[[1]]$A)[8:10]),
     Topping=c("Pepperoni",
               colnames(pizzavd[[1]]$A)[11:15]),
     Spread=c("Moderate",
              colnames(pizzavd[[1]]$A)[16]),
     Cheese=c('No claim',
              colnames(pizzavd[[1]]$A)[17]))
```




Dummy coding: Pizza Example
========================================================

```r
pizzalevels
```

```
$Brand
[1] "DiGiorno" "Fresc"    "RedBa"    "Priv"     "Tomb"     "Tony"    

$Size
[1] "One"    "ForTwo"

$Crust
[1] "Thin"   "TrCr"   "StufCr" "RisCr" 

$Topping
[1] "Pepperoni" "Cheese"    "Veg"       "Surp"      "PepSauHam" "HI"       

$Spread
[1] "Moderate" "densetop"

$Cheese
[1] "No claim"   "realcheese"
```






Dummy coding: Ice cream Example Design Matrix
========================================================


```r
ICdata[[1]][[1]]$A[1:9,1:5]
```

```
      bl BenNJerry BlueBell BlueBunny Dryers
 [1,]  1         0        0         0      0
 [2,]  1         0        0         0      0
 [3,]  1         1        0         0      0
 [4,]  1         1        0         0      0
 [5,]  1         0        0         0      0
 [6,]  1         0        0         0      0
 [7,]  1         0        0         1      0
 [8,]  1         0        0         1      0
 [9,]  1         0        0         0      0
```




Dummy coding: Ice cream Example
========================================================

Full list of attributes and levels:

```r
list(Brand=c("Breyers",colnames(ICdata[[1]][[1]]$A[,2:7])),
     Flavor=c("Vanilla",colnames(ICdata[[1]][[1]]$A[,8:16])),
     Size=c('size4',colnames(ICdata[[1]][[1]]$A[,17:18])))
```

```
$Brand
[1] "Breyers"    "BenNJerry"  "BlueBell"   "BlueBunny"  "Dryers"    
[6] "HaagenDaz"  "StoreBrand"

$Flavor
 [1] "Vanilla"              "Chocolate"            "ChocChip"            
 [4] "ChocoChipCookieDough" "CookiesCream"         "Neapolitan"          
 [7] "Oreo"                 "RockyRoad"            "VanillaBean"         
[10] "VanillaFudgeRipple"  

$Size
[1] "size4"  "size8"  "size16"
```




Estimation
========================================================
incremental: true

- Main function `rVDMevs(Data, Prior, Mcmc)` mimics bayesm functions

- Arguments
  + Data	  -- contains a list of (voldata, and possibly Z)
  + Prior	  -- contains a list of (Deltabar, ADelta, nu, V); defaults should work for most users
  + Mcmc	  -- is a list of (R, keep, steptunestart, tunelength, tuneinterval, livetraceplot, thetastart, truevals); `Mcmc=list(R=80000)` should be defined
  
- Automatic tuning is used to achieve .7-ish rejection rate for Metropolis-Hastings algorithm
  + By default, first 1000 draws are used and need to be removed when summarizing posterior
  



Estimation output
========================================================
incremental: true

Output from `rVDMevs` is a list with the following

- `thetaDraw`: An array of dimension `Ndraws` X `Nunits` X `p` where `p` is he number of model parameters
- `GammaDraw`: A matrix of dimension `Ndraws` X `p`*`m`
- `SigmaThetaDraw`: An array of dimension `Ndraws` X `p` X `p` 
- `logLike`: A vector of length `Ndraws` containing the log-likelihood values
- `reject`: A vector of length `Ndraws` containing the averaged rejection rate of the unit-level Metropolis-Hastings sampler








Example 1: Frozen Pizza
====================================
type: section






Workflow
========================================================
incremental: true

- (1) Data selection
- (2) Model Estimation
- (3) Assessing MCMC output
- (4) Parameter estimates
- (5) Evaluate fit
- (6) Predictions, Market simulation







(1) Data selection - Pizza
========================================================
incremental: true

- `vd_voldataSplitHoldout` helps split `voldata` data into 
  + an in-sample portion that is used for analysis, and 
  + an out-of-sample portion (sometimes called hold-out)
- Arguments
  + `voldata`: list of lists
  + `nho`: number of hold out tasks to keep

<!-- code we want to run -- this is what is shown in the document to the reader -->

```r
#split into in-sample and out-of-sample
voldata_demo_split=vd_voldataSplitHoldout(voldata_demo$voldata, 2)
```
<!-- code actually run -- not shown to the reader, done to suppress output -->



```r
names(voldata_demo_split)
```

```
[1] "voldata_in"  "voldata_out" "intasks"     "outtasks"   
```




(2) Model Estimation - Pizza
========================================================
incremental: true

- The first argument of `rVDMevs` is `Data`. It needs to contain a list that contains `voldata`, and if desired, covariates for an upper-level regression model. 
- The second argument `Mcmc` is a list, and at least the number of draws `R` should be set. 
- The `prior` argument does not need to be specified, as default prior settings should work for most users.

<!-- code we want to run -- this is what is shown in the document to the reader -->

```r
outPizza = rVDMevs(Data=list(voldata=voldata_demo_split$voldata_in), 
                   Mcmc=list(R=10000, livetraceplot=T))
```

<!-- code actually run -- not shown to the reader, done to suppress output -->




(3) Assessing MCMC output - Pizza
========================================================


```r
str(outPizza)
```

```
List of 5
 $ thetaDraw     : num [1:2000, 1:200, 1:20] -0.846 -0.846 -0.846 -0.846 -0.846 ...
  ..- attr(*, "dimnames")=List of 3
  .. ..$ : NULL
  .. ..$ : NULL
  .. ..$ : chr [1:20] "" "Fresc" "RedBa" "Priv" ...
 $ GammaDraw     : num [1:2000, 1:20] -1.09 -1.14 -1.26 -1.45 -1.52 ...
  ..- attr(*, "dimnames")=List of 2
  .. ..$ : NULL
  .. ..$ : chr [1:20] "" "Fresc" "RedBa" "Priv" ...
 $ SigmaThetaDraw: num [1:2000, 1:20, 1:20] 0.16 0.209 0.37 0.446 0.438 ...
  ..- attr(*, "dimnames")=List of 3
  .. ..$ : NULL
  .. ..$ : chr [1:20] "" "Fresc" "RedBa" "Priv" ...
  .. ..$ : chr [1:20] "" "Fresc" "RedBa" "Priv" ...
 $ logLike       : num [1:2000] -12281 -10960 -10410 -9981 -9607 ...
 $ reject        : num [1:2000] NA NA 0.954 0.954 0.954 ...
```




(3) Assessing MCMC output - Pizza
========================================================

- 'Ocular inspection' of convergence
<img src="vdmtut-figure/unnamed-chunk-9-1.png" title="plot of chunk unnamed-chunk-9" alt="plot of chunk unnamed-chunk-9" style="display: block; margin: auto;" />




(3) Assessing MCMC output - Pizza
========================================================

- Diagnostics, such as Geweke's convergence diagnostic
- In practice, not used regularly


```r
#install.packages('coda')
library(coda)
geweke.diag(mcmc(outPizza$GammaDraw))
```


(3) Assessing MCMC output - Pizza
========================================================

- Rejection Rate should be in the .7-.8 range for Metropolis-Hastings algorithm
- Automatic tuning over 1000 draws achieves that for many data sets I have seen



```r
matplot(outPizza$reject,type='l')
```

![plot of chunk unnamed-chunk-11](vdmtut-figure/unnamed-chunk-11-1.png)




(4) Parameter estimates - Pizza
========================================================

- Arrange data for ggplot2


```r
ddf=data.frame(beta=apply(outPizza$thetaDraw[-(1:100),,1:17],3,mean),
               name=factor(dimnames(outPizza$thetaDraw)[[3]][1:17],
                           levels=dimnames(outPizza$thetaDraw)[[3]]),
               group=factor( c('bl', rep('brand',5),'size',rep('crust',3),
                               rep('topping',5),'spread','cheese'),
                             levels=c("bl","brand", "size", "crust","topping","spread","cheese") ))
```




(4) Parameter estimates - Pizza
========================================================

- Generate ggplot2 graph


```r
p=ggplot(ddf,aes(x=name,y=beta,fill=group))+  geom_bar(stat='identity') +  coord_flip() +facet_grid(group~., scales = 'free', space = 'free')  + guides(fill=FALSE) + theme(  axis.title.y = element_blank(), text = element_text(size=24))
```




(4) Parameter estimates - Pizza
========================================================

<img src="vdmtut-figure/unnamed-chunk-14-1.png" title="plot of chunk unnamed-chunk-14" alt="plot of chunk unnamed-chunk-14" style="display: block; margin: auto;" />




(5) Evaluate fit: In-sample fit - Pizza
========================================================

- Evaluating model fit, we can look at in-sample and out-of-sample fit.
- In-sample fit is usually evaluated in terms of the marginal likelihood. 
  + The most simple approximation is using the Newton-Raftery method, implemented in `bayesm`
  

```r
logMargDenNR(rev(outPizza$logLike)[1:100])
```

```
[1] -4986.801
```




(5) Evaluate fit: Out-of-sample fit - Pizza
========================================================
incremental: true

- We left 2 choice tasks as 'hold-out', saved in the `ICdata_split` object
- Create draws from the posterior distribution of demand using the `VDMevs_demand` function

```r
voldata_demo_oosdemand = VDMevs_demand(outPizza$thetaDraw[-(1:100),,], 
                                       voldata_demo_split$voldata_out)
```

```
Generating draws from unit-level posterior demand distributions
```
- The function generates a list with `N=200` matrices (`ntask`X`nalt`X`ndraws`), which contain draws from posterior of demand

```r
length(voldata_demo_oosdemand)
```

```
[1] 200
```

```r
str(voldata_demo_oosdemand[1:3])
```

```
List of 3
 $ : num [1:2, 1:6, 1:1900] 0 0 0.142 0 0 ...
 $ : num [1:2, 1:6, 1:1900] 0 0 0 0 0 ...
 $ : num [1:2, 1:6, 1:1900] 0 0 0 0 0 0 0 0 0 0 ...
```




(5) Evaluate fit: Out-of-sample fit - Pizza
========================================================
incremental: true

- Obtain posterior mean demand estimates using `vdm_demand_pm`

```r
voldata_demo_oosdemandPM = vdm_demand_pm(voldata_demo_oosdemand)
```

- Then evaluate fit using the `vd_predeval_indiv` function
  + Includes MSE, MAE and RAE (mean-squared error, mean-absolute error and relative absolute error)
  + RAE puts the MAE in relation to the average demand quantity, should be <1

```r
  colMeans(vd_predeval_indiv(voldata_demo_oosdemandPM, voldata_demo_split$voldata_out),na.rm=T)
```

```
      mse       mae       rae      rmse 
3.3758345 1.2323922 0.5474108 0.7260794 
```




(5) Evaluate fit: In-sample fit (again) - Pizza
========================================================
incremental: true

- We can also apply these classic fit measures (MSE, MAE) to evaluate in-sample fit
- First, we generate draws from the posterior distribution of demand

```r
voldata_demo_indemand   = VDMevs_demand(outPizza$thetaDraw, voldata_demo_split$voldata_in)
```

```
Generating draws from unit-level posterior demand distributions
```
- Then we compute posterior mean demand estimates and compute fit statistics

```r
voldata_demo_indemandPM = vdm_demand_pm(voldata_demo_indemand)
colMeans(vd_predeval_indiv(voldata_demo_indemandPM, voldata_demo_split$voldata_in),na.rm=T)
```

```
      mse       mae       rae      rmse 
4.1632250 1.3933345 0.5851023 0.7546941 
```






(6) Market simulation - Pizza
========================================================

Define design matrix

```r
desmat1=cbind(1, 0,0,1,0,0    ,1,   1,0,0,  diag(6)[,-1] ,   1,1)
colnames(desmat1)=c('',c("Fresc", "RedBa", "Priv", "Tomb", "Tony", "ForTwo", "TrCr", 
                         "StufCr", "RisCr", "Cheese", "Veg", "Surp", "PepSauHam", "HI", 
                         "densetop", "realcheese"))
nalt=6
```




(6) Market simulation - Pizza
========================================================


|   | Fresc| RedBa| Priv| Tomb| Tony| ForTwo| TrCr| StufCr| RisCr| Cheese| Veg| Surp| PepSauHam| HI| densetop| realcheese|
|--:|-----:|-----:|----:|----:|----:|------:|----:|------:|-----:|------:|---:|----:|---------:|--:|--------:|----------:|
|  1|     0|     0|    1|    0|    0|      1|    1|      0|     0|      0|   0|    0|         0|  0|        1|          1|
|  1|     0|     0|    1|    0|    0|      1|    1|      0|     0|      1|   0|    0|         0|  0|        1|          1|
|  1|     0|     0|    1|    0|    0|      1|    1|      0|     0|      0|   1|    0|         0|  0|        1|          1|
|  1|     0|     0|    1|    0|    0|      1|    1|      0|     0|      0|   0|    1|         0|  0|        1|          1|
|  1|     0|     0|    1|    0|    0|      1|    1|      0|     0|      0|   0|    0|         1|  0|        1|          1|
|  1|     0|     0|    1|    0|    0|      1|    1|      0|     0|      0|   0|    0|         0|  1|        1|          1|




(6) Market simulation - Pizza
========================================================

Set prices

```r
simprices=matrix(c(4,4,3,3,3,2),1,6)
```

Set sample size:

```r
N=200
```




(6) Market simulation - Pizza
========================================================

Generate `voldata` style list of lists:

```r
pizza_msim_1=list()
for(ii in 1:N){
  pizza_msim_1[[ii]]=list()
  pizza_msim_1[[ii]]$A = desmat1
  pizza_msim_1[[ii]]$P = simprices
  pizza_msim_1[[ii]]$X = matrix(NA,1,nalt)
}
```




(6) Market simulation - Pizza
========================================================

- Generate draws from posterior of demand for the household-level

```r
voldata_demo_msim1  = VDMevs_demand(outPizza$thetaDraw, pizza_msim_1)
```

```
Generating draws from unit-level posterior demand distributions
```



(6) Market simulation - Pizza
========================================================

- Generate draws from posterior of demand for the household-level

```r
outPizzaMarket = vdmevs_thetaGeneralize(drawout=outPizza, nunits=200,  ndraws=1000)
voldata_demo_msim1  = VDMevs_demand(outPizzaMarket, pizza_msim_1)
```






(6) Market simulation - Pizza
========================================================
incremental: true

- Aggregate (market-level) demand is obtained using the `vdm_uprodagg` function. 
  + It identifies all unique products across respondents, and generates posterior draws of aggregate demand
  + In this case, only 6 products exist, and all units are dealing with the same products


```r
voldata_demo_msim1AGGDEMAND=vdm_uprodagg(pizza_msim_1, voldata_demo_msim1)
```

- Two arguments:
  + voldata = pizza_msim_1 - voldata design matrices, prices
  + dempostd = voldata_demo_msim1 - hh level draws of posterior of demand



(6) Market simulation - Pizza
========================================================

- It is straightforward to obtain posterior means of aggregate demand and generate a barplot


```r
ddf=data.frame(demand=rowMeans(voldata_demo_msim1AGGDEMAND))
ddf$product=rownames(ddf)
p=ggplot(ddf,aes(x=product,y=demand))+geom_bar(stat='identity')  + coord_flip() + theme(text = element_text(size=24))
```




(6) Market simulation - Pizza
========================================================

<img src="vdmtut-figure/unnamed-chunk-29-1.png" title="plot of chunk unnamed-chunk-29" alt="plot of chunk unnamed-chunk-29" style="display: block; margin: auto;" />



(6) Market simulation - another example - Pizza
========================================================

- Let's expand the product offerings from 6 to 9
- Generate 3 more sets of design matrices
  + containing 7,8,9 alternatives



(6) Market simulation - another example - Pizza
========================================================


```r
desmat1a=rbind(desmat1,desmat1[1,]+c(0,1,rep(0,15)))
desmat1b=rbind(desmat1a,desmat1[2,]+c(0,1,rep(0,15)))
desmat1c=rbind(desmat1b,desmat1[3,]+c(0,1,rep(0,15)))

simpricesa=matrix(c(4,4,3,3,3,2,4),1,7)
simpricesb=matrix(c(4,4,3,3,3,2,4,4),1,8)
simpricesc=matrix(c(4,4,3,3,3,2,4,4,3),1,9)
```




(6) Market simulation - another example - Pizza
========================================================

- Generate `voldata` style lists


```r
pizza_msim_1a=list();pizza_msim_1b=list();pizza_msim_1c=list()
for(ii in 1:N){
  pizza_msim_1a[[ii]]=list();    pizza_msim_1a[[ii]]$A= desmat1a
  pizza_msim_1a[[ii]]$P= simpricesa;    pizza_msim_1a[[ii]]$X= matrix(NA,1,nalt+1)
}
for(ii in 1:N){
  pizza_msim_1b[[ii]]=list();    pizza_msim_1b[[ii]]$A= desmat1b
  pizza_msim_1b[[ii]]$P= simpricesb;    pizza_msim_1b[[ii]]$X= matrix(NA,1,nalt+2)
}
for(ii in 1:N){
  pizza_msim_1c[[ii]]=list();    pizza_msim_1c[[ii]]$A= desmat1c
  pizza_msim_1c[[ii]]$P= simpricesc;    pizza_msim_1c[[ii]]$X= matrix(NA,1,nalt+3)
}
```



(6) Market simulation - another example - Pizza
========================================================

- Obtain demand estimates for 6,7,8,9 alternatives present


```r
  voldata_demo_msim1  = VDMevs_demand(outPizza$thetaDraw, pizza_msim_1)
```

```
Generating draws from unit-level posterior demand distributions
```

```r
  voldata_demo_msim1a  = VDMevs_demand(outPizza$thetaDraw, pizza_msim_1a)
```

```
Generating draws from unit-level posterior demand distributions
```

```r
  voldata_demo_msim1b  = VDMevs_demand(outPizza$thetaDraw, pizza_msim_1b)
```

```
Generating draws from unit-level posterior demand distributions
```

```r
  voldata_demo_msim1c  = VDMevs_demand(outPizza$thetaDraw, pizza_msim_1c)
```

```
Generating draws from unit-level posterior demand distributions
```

```r
  vdmagg0=rowMeans(vdm_uprodagg(pizza_msim_1,voldata_demo_msim1))
  vdmagg1=rowMeans(vdm_uprodagg(pizza_msim_1a,voldata_demo_msim1a))
  vdmagg2=rowMeans(vdm_uprodagg(pizza_msim_1b,voldata_demo_msim1b))
  vdmagg3=rowMeans(vdm_uprodagg(pizza_msim_1c,voldata_demo_msim1c))
```



(6) Market simulation - another example - Pizza
========================================================

- Product demand given the 4 scenarios

<img src="vdmtut-figure/unnamed-chunk-33-1.png" title="plot of chunk unnamed-chunk-33" alt="plot of chunk unnamed-chunk-33" style="display: block; margin: auto;" />




(6) Market simulation - another example - Pizza
========================================================

- Primary demand given these 4 scenarios

<img src="vdmtut-figure/unnamed-chunk-34-1.png" title="plot of chunk unnamed-chunk-34" alt="plot of chunk unnamed-chunk-34" style="display: block; margin: auto;" />











Example 1: Ice cream
====================================
type: section




Workflow (again)
========================================================

- (1) Data selection
- (2) Model Estimation
- (3) Assessing MCMC output
- (4) Parameter estimates
- (5) Evaluate fit
- (6) Predictions, Market simulation




Ice Cream data
========================================================

<div class="midcenter" style="margin-left:-300px; margin-top:-300px;">
<img src="icecream.png"></img>
</div>



Ice Cream data - attribute levels
========================================================

Full list of attributes and levels:

```r
list(Brand=c("Breyers",colnames(ICdata[[1]][[1]]$A[,2:7])),
     Flavor=c("Vanilla",colnames(ICdata[[1]][[1]]$A[,8:16])),
     Size=c('size4',colnames(ICdata[[1]][[1]]$A[,17:18])))
```

```
$Brand
[1] "Breyers"    "BenNJerry"  "BlueBell"   "BlueBunny"  "Dryers"    
[6] "HaagenDaz"  "StoreBrand"

$Flavor
 [1] "Vanilla"              "Chocolate"            "ChocChip"            
 [4] "ChocoChipCookieDough" "CookiesCream"         "Neapolitan"          
 [7] "Oreo"                 "RockyRoad"            "VanillaBean"         
[10] "VanillaFudgeRipple"  

$Size
[1] "size4"  "size8"  "size16"
```




Ice Cream data - how to deal with quantity
========================================================

- We express demand in terms of units of 4 ounces, not in 'number of packs'
- Limited bias of indivisiblity (when only certain pack sizes are offered)
- Pack size attribute is preference for container size, not quantity




(1) Data selection - ice cream
========================================================

- We may wish to exclude extreme responses from the analysis
- The following command exludes respondents who on average choose quantities exceeding 15*4=60oz per choice task. This removes 11 of the 601 respondents.


```r
filter=(sapply(ICdata$voldata,function(x)mean(rowSums(x$X)))<15)
ICdata$voldata=ICdata$voldata[filter]
```



(1) Data selection - ice cream
========================================================

- `vd_voldataSplitHoldout` helps split `voldata` data into 
  + an in-sample portion that is used for analysis, and 
  + an out-of-sample portion (sometimes called hold-out)
- Arguments
  + `voldata`: list of lists
  + `nho`: number of hold out tasks to keep

<!-- code we want to run -- this is what is shown in the document to the reader -->

```r
#split into in-sample and out-of-sample
ICdata_split=vd_voldataSplitHoldout(ICdata$voldata, 2)
```
<!-- code actually run -- not shown to the reader, done to suppress output -->




```r
names(ICdata_split)
```

```
[1] "voldata_in"  "voldata_out" "intasks"     "outtasks"   
```



(2) Model Estimation - ice cream
========================================================

- The first argument of `rVDMevs` is `Data`. It needs to contain a list that contains `voldata`, and if desired, covariates for an upper-level regression model. 
- The second argument `Mcmc` is a list, and at least the number of draws `R` should be set. 
- The `prior` argument does not need to be specified, as default prior settings should work for most users.

<!-- code we want to run -- this is what is shown in the document to the reader -->

```r
#estimate model
outIC = rVDMevs(Data=list(voldata=ICdata_split$voldata_in),
                Mcmc=list(R=20000,livetraceplot=T))
#increase R to 80000 for more accurate results
```

<!-- code actually run -- not shown to the reader, done to suppress output -->






(2) Model Estimation Output - ice cream
========================================================


```r
str(outIC)
```

```
List of 5
 $ thetaDraw     : num [1:4000, 1:507, 1:21] -1 -1.51 -1.56 -1.56 -1.56 ...
  ..- attr(*, "dimnames")=List of 3
  .. ..$ : NULL
  .. ..$ : NULL
  .. ..$ : chr [1:21] "bl" "BenNJerry" "BlueBell" "BlueBunny" ...
 $ GammaDraw     : num [1:4000, 1:21] -1.17 -1.4 -1.62 -1.81 -2 ...
  ..- attr(*, "dimnames")=List of 2
  .. ..$ : NULL
  .. ..$ : chr [1:21] "bl" "BenNJerry" "BlueBell" "BlueBunny" ...
 $ SigmaThetaDraw: num [1:4000, 1:21, 1:21] 0.14 0.242 0.342 0.521 0.506 ...
  ..- attr(*, "dimnames")=List of 3
  .. ..$ : NULL
  .. ..$ : chr [1:21] "bl" "BenNJerry" "BlueBell" "BlueBunny" ...
  .. ..$ : chr [1:21] "bl" "BenNJerry" "BlueBell" "BlueBunny" ...
 $ logLike       : num [1:4000] -61130 -51432 -42911 -37778 -33786 ...
 $ reject        : num [1:4000] NA NA 0.921 0.921 0.921 ...
```




(3) Assessing MCMC output - ice cream
========================================================

- 'Ocular inspection'

<img src="vdmtut-figure/unnamed-chunk-43-1.png" title="plot of chunk unnamed-chunk-43" alt="plot of chunk unnamed-chunk-43" style="display: block; margin: auto;" />



(3) Assessing MCMC output - ice cream
========================================================

- Diagnostics, such as Geweke's convergence diagnostic


```r
#install.packages('coda')
library(coda)
geweke.diag(mcmc(outIC$GammaDraw))
```




(4) Parameter estimates - ice cream
========================================================

- Assessing part-worths, satiation, budget and error scales is straightforward. Given a stndard Multivariate Normal prior without covariates, one can simply average the upper level $bar\theta$s in `GammaDraw`:


```r
colMeans(outIC$GammaDraw[-(1:900),])
```

```
                  bl            BenNJerry             BlueBell 
         -3.36714710          -0.25209173          -0.85673671 
           BlueBunny               Dryers            HaagenDaz 
         -0.75498821          -0.70304055          -0.38458541 
          StoreBrand            Chocolate             ChocChip 
         -0.56265859          -0.40434124          -0.38485734 
ChocoChipCookieDough         CookiesCream           Neapolitan 
         -0.40165303          -0.39268934          -0.83683061 
                Oreo            RockyRoad          VanillaBean 
         -0.63300666          -0.40330297          -0.03561045 
  VanillaFudgeRipple                size8               size16 
         -0.36551442          -0.25764594          -0.28282143 
                ln_g                 ln_E               ln_sig 
         -2.41395935           2.24631049          -0.46104792 
```





(4) Parameter estimates - ice cream
========================================================

- If an upper-level regression was used, or the code was modified to incorporate a more fleixble prior specification, useres can summarize averages over the individual beta estimates
- Also, plots can be generated using `ggplot2`





(4) Parameter estimates: Part-worth visualization
========================================================



```r
ddf=data.frame(beta=apply(outIC$thetaDraw[-(1:900),,1:18],3,mean),
               name=factor(dimnames(outIC$thetaDraw)[[3]][1:18],
                           levels=dimnames(outIC$thetaDraw)[[3]]),
               group=factor( c('bl', rep('brand',6),rep('flavor',9),rep('size',2)),
                             levels=c("bl","brand", "flavor", "size") ))
 
p=ggplot(ddf,aes(x=name,y=beta,fill=group))+  geom_bar(stat='identity') +  coord_flip() +facet_grid(group~., scales = 'free', space = 'free')  + guides(fill=FALSE) + theme(  axis.title.y = element_blank())
```




(4) Parameter estimates: Part-worth visualization
========================================================

<img src="vdmtut-figure/unnamed-chunk-47-1.png" title="plot of chunk unnamed-chunk-47" alt="plot of chunk unnamed-chunk-47" style="display: block; margin: auto;" />




(5) Evaluate fit: In-sample fit - ice cream
========================================================

- Evaluating model fit, we can look at in-sample and out-of-sample fit.

- In-sample fit is often evaluated in terms of the marginal likelihood. THe most simple approximation is using the Newton-Raftery method, implemented in `bayesm`.

```r
logMargDenNR(rev(outIC$logLike)[1:1000])
```

```
[1] -16298.54
```




(5) Evaluate fit: Out-of-sample fit - ice cream
========================================================

- We left 2 choice tasks as 'hold-out', saved in the `ICdata_split` object
- Create draws from the posterior distribution of demand using the `VDMevs_demand` function

```r
ICoosdemand   = VDMevs_demand(vdm_thin_drawobject(outIC,1000)$thetaDraw, 
                              ICdata_split$voldata_out)
```

```
Generating draws from unit-level posterior demand distributions
```
- Obtain posterior mean demand estimates using `vdm_demand_pm`

```r
ICoosdemandPM = vdm_demand_pm(ICoosdemand)
```




(5) Evaluate fit: Out-of-sample fit - ice cream
========================================================

- Then evaluate fit using the `vd_predeval_indiv` function
  + IncludesMSE, MAE and RAE (mean-squared error, mean-absolute error and relative absolute error)
  + RAE puts the MAE in relation to the average demand quantity, should be <1
  

```r
colMeans(vd_predeval_indiv(ICoosdemandPM, ICdata_split$voldata_out),na.rm=T)
```

```
      mse       mae       rae      rmse 
0.7611930 0.7160014 0.7905411 0.3194963 
```




(5) Evaluate fit: In-sample fit (again) - ice cream
========================================================

- We can also apply these classic fit measures (MSE, MAE) t0 evaluate in-sample fit
- First, we generate draws from the posterior distribution of demand

```r
ICoosdemand   = VDMevs_demand(vdm_thin_drawobject(outIC,1000)$thetaDraw, ICdata_split$voldata_in)
```

```
Generating draws from unit-level posterior demand distributions
```
- Then we compute posterior mean demand estimates and compute fit statistics

```r
ICoosdemandPM = vdm_demand_pm(ICoosdemand)
colMeans(vd_predeval_indiv(ICoosdemandPM, ICdata_split$voldata_in),na.rm=T)
```

```
      mse       mae       rae      rmse 
0.4198766 0.5862209 0.6058303 0.2428582 
```



(6) Elasticities
========================================================

- Set offerings for the 507 customers
- For demo purposes, we just use offerings of 1st task from 1st respondent

```r
A1=ICdata$voldata[[1]]$A[1:12,]
P1=ICdata$voldata[[1]]$P[1,,drop=F]
voldata_sim=list()
for(ii in 1:507){
voldata_sim[[ii]]=list()
voldata_sim[[ii]]$A=A1
voldata_sim[[ii]]$P=P1}
```




(6) Elasticities
========================================================

- Thin the draws of the 507 respondents to speed up for demo purposes
  + However, use 10.000+ for accurate results
- Obtain elasticities using the `vdmevs_elast` function

```r
elasIC   = vdmevs_elast(vdm_thin_drawobject(outIC,500)$thetaDraw, 
                        voldata_sim)
elasICPMr=round(apply(elasIC,1:2,mean,na.rm=T),2)
```

- Show own-elasticities

```r
diag(elasICPMr)
```

```
100000100001000001 100000100000001001 110000000000100001 
             -1.41              -1.39              -1.31 
110000000001000001 100001000000010000 100001000000100001 
             -1.27              -1.44              -1.42 
100100000000100001 100100000001000000 100000000000010001 
             -1.49              -1.18              -1.43 
100000000000000000 100010010000000001 100010000001000010 
             -1.28              -1.45              -0.42 
```




(6) Cross-elasticities
========================================================


|      |      |      |      |      |      |      |      |      |      |      |      |
|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|
| -1.41|  0.06|  0.36|  0.45|  0.31|  0.48|  0.36|  0.62|  0.16|  0.91|  0.49|  1.63|
|  0.50| -1.39|  0.44|  0.37|  0.42|  0.49|  0.58|  0.86|  0.19|  0.73|  0.39|  1.51|
|  0.49|  0.05| -1.31|  0.48|  0.34|  0.58|  0.43|  0.73|  0.18|  0.77|  0.48|  1.88|
|  0.53|  0.09|  0.30| -1.27|  0.25|  0.43|  0.53|  0.77|  0.19|  0.97|  0.32|  1.60|
|  0.51|  0.05|  0.45|  0.44| -1.44|  0.24|  0.35|  0.70|  0.27|  0.79|  0.37|  1.17|
|  0.46|  0.07|  0.51|  0.49|  0.43| -1.42|  0.56|  0.76|  0.13|  0.74|  0.42|  1.57|
|  0.56|  0.16|  0.45|  0.40|  0.42|  0.40| -1.49|  0.58|  0.12|  0.75|  0.29|  1.71|
|  0.43|  0.05|  0.26|  0.32|  0.41|  0.58|  0.50| -1.18|  0.08|  0.80|  0.27|  1.54|
|  0.55|  0.11|  0.43|  0.44|  0.46|  0.48|  0.38|  0.93| -1.43|  0.73|  0.26|  1.64|
|  0.50|  0.08|  0.39|  0.40|  0.24|  0.36|  0.37|  0.87|  0.08| -1.28|  0.28|  1.40|
|  0.62| -0.01|  0.27|  0.30|  0.25|  0.58|  0.42|  0.39|  0.20|  0.84| -1.45|  1.43|
|  0.68|  0.07|  0.39|  0.42|  0.30|  0.26|  0.45|  0.75|  0.20|  0.75|  0.41| -0.42|






(6) Demand curve
========================================================

- Set a 'baseline market' with 100 customers

```r
A1=ICdata$voldata[[1]]$A[1:12,]
P1=ICdata$voldata[[1]]$P[1,,drop=F]
voldata_sim=list()
for(ii in 1:10){
voldata_sim[[ii]]=list()
voldata_sim[[ii]]$A=A1
voldata_sim[[ii]]$P=P1}
```

- Define focal product out of the 12 available

```r
focal=as.character(paste0(voldata_sim[[1]]$A[1,],collapse = ""))
```

- Generate 'market' draws

```r
outICMarket = vdmevs_thetaGeneralize(drawout=outIC, nunits=10,  ndraws=5000)
```




(6) Demand curve (2)
========================================================


```r
dd=vdmevs_demcurve(thetaDraw = outICMarket,
                   voldata = voldata_sim,
                   demcurveprodIDs = focal )

ddf=data.frame(discount=dd$price,  EMS=dd$EVol)

p=ggplot(ddf,aes(x=discount,y=EMS)) +geom_line(size=1.1) + ggtitle('Demand curve') +
  labs(y = 'Market Share', x='Price as proportion of current price')  + 
  theme_bw()+ theme(plot.title = element_text(hjust = 0.5))
```




(6) Demand curve (3)
========================================================

<img src="vdmtut-figure/unnamed-chunk-62-1.png" title="plot of chunk unnamed-chunk-62" alt="plot of chunk unnamed-chunk-62" style="display: block; margin: auto;" />









Exploded Logit
====================================
type: section




Can I 'logit' it?
========================================================

- `datalist_vdm2logit` converts format to 'exploded' logit format
- For example, if a customer buys 2 product A and 3 product B, there will be 5 discrete decisions
- Empirically, part-worth estimates are often similar
- However, satiation and budget are not estimated
- No prediction of quantity (how to know there are 5 decisions)




Explode volumes
========================================================

- We can 'explode' choices only if demand is in integers (not continous)

```r
IC_lgtdata=datalist_vdm2logit(ICdata$voldata)
```

- Data is in familiar lgtdata format

```r
str(IC_lgtdata[[1]])
```

```
List of 2
 $ y: int [1:51] 1 1 1 1 1 1 1 1 2 2 ...
 $ X: num [1:663, 1:19] 1 1 1 1 1 1 1 1 1 1 ...
  ..- attr(*, "dimnames")=List of 2
  .. ..$ : NULL
  .. ..$ : chr [1:19] "bl" "BenNJerry" "BlueBell" "BlueBunny" ...
```




Using rhierMnlRwMixture
========================================================

- Estimation via `rhierMnlRwMixture` from bayesm
- Sign constraint added, number of option needs to be specified (12+outside)

<!-- code we want to run -- this is what is shown in the document to the reader -->

```r
Mcmc1  = list(R=10000, keep=5)
Prior1 = list(ncomp=1, SignRes=c(rep(0,18),-1))
out_IC_logit = rhierMnlRwMixture(Data=list(p=13,lgtdata=IC_lgtdata), 
                                 Prior=Prior1, 
                                 Mcmc=Mcmc1)
```


<!-- code actually run -- not shown to the reader, done to suppress output -->




Scatterplotting logit vs volumetric part-worths
========================================================


```r
IC_logit_beta=apply(out_IC_logit$betadraw,2,mean)
IC_VDM_theta=apply(outIC$thetaDraw,3,mean)

ddf=data.frame(logit=IC_logit_beta[2:18],vdm=IC_VDM_theta[2:18],
               lvl=names(IC_VDM_theta)[2:18],
               atr=c(rep('brand',6),rep('flavor',9),rep('size',2)))

p=ggplot(ddf,aes(x=logit,y=vdm,color=atr))+geom_point()
```




Exploded Logit vs Volumetric
========================================================

- Overall, part-worth are correlated, except on package size

<img src="vdmtut-figure/unnamed-chunk-68-1.png" title="plot of chunk unnamed-chunk-68" alt="plot of chunk unnamed-chunk-68" style="display: block; margin: auto;" />


